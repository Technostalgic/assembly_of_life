; Isaiah Smith
; 4/20/24
; Set up and begin program execution

; 8086 code of version 386
.386P

; specify memory model
.model flat

extern _init_io: near
extern _insert_new_line: near

extern _init_commands: near
extern _command_loop: near
extern _cmd_help: near
extern _cmd_grid: near

; data section is used to store data such as global variables
.data

; code section contains assembly instructions
.code

; initialize program logic
initialize PROC C
_initialize:

	; initialize modules
	call _init_io
	call _init_commands

	; show grid and help message
	call _cmd_grid
	call _cmd_help

	; begin program command loop
	call _command_loop

	ret

initialize ENDP

END