; Isaiah Smith
; 4 / 20 / 24
; Provide functionality for parsing and executing commands in the cli for the program

; 8086 code of version 386
.386P

; specify memory model
.model flat

extern _ExitProcess@4: near
extern _Sleep@4: near

extern _reset_cursor: near
extern _clear_space: near
extern _insert_new_line: near
extern _write_str: near
extern _read_line: near
extern _hash_str: near
extern _write_int: near
extern _seeded_rand: near

extern _grid_get_cell: near
extern _grid_set_cell: near
extern _grid_next_cell_state: near
extern _grid_step: near
extern _draw_grid: near

; data section is used to store data such as global variables
.data

input_prompt	byte	"Enter command:", 10, "> ", 0

; strings used for displaying info to the user from commands
echo_help		db	"Available Commands: (Command, 'Example', Description)", 10
				db		"	help		'help'		list available commands", 10
				db		"	grid		'grid'		display the simulation grid", 10
				db		"	clear		'clear'		clear all living cells from the grid", 10
				db		"	rand <number>	'rand 652'		toggle random cells on the grid, using <number> as a seed", 10
				db		"	<coordinate>	'f12'		toggle the cell alive/dead at <coordinate>", 10
				db		"	*empty*		''		step simulation once", 10
				db		"	<number>	'50'		step simulation <number> times", 10
				db		"	rate <number>	'rate 250'		set the delay between simulation steps, in milliseconds", 10
				db		"	quit		'quit'		exit the program", 10
				db		0
echo_step		byte	"step ", 0
echo_step_2		byte	" out of ", 0
echo_grid		byte	"grid", 0
echo_clear		byte	"clear all cells", 0
echo_toggle		byte	"toggle ", 0
echo_rand		byte	"randomize with seed ", 0
echo_rate		byte	"animation rate set to ", 0
echo_invalid	byte	"invalid command", 0
echo_quit		byte	"exit program", 0

; command strings and hashes
str_help		byte	"help", 0
str_grid		byte	"grid", 0
str_clear		byte	"clear", 0
str_rand		byte	"rand", 0
str_rate		byte	"rate", 0
str_quit		byte	"quit", 0
hash_help		dword	0
hash_grid		dword	0
hash_clear		dword	0
hash_rand		dword	0
hash_rate		dword	0
hash_quit		dword	0

rand_seed		dword	0
anim_delay		dword	100

; the command buffer where the whole command will be stored
command_buffer	byte	512	DUP(0)

; the parsed integer argument passed to the command
arg_int			dword	0

; code section contains assembly instructions
.code

; initialize the command data
init_commands PROC C
_init_commands:

	; compute hash for help command
	push offset str_help
	call _hash_str
	mov hash_help, eax
	
	; compute hash for grid command
	push offset str_grid
	call _hash_str
	mov hash_grid, eax
	
	; compute hash for clear command
	push offset str_clear
	call _hash_str
	mov hash_clear, eax

	; compute hash for rand command
	push offset str_rand
	call _hash_str
	mov hash_rand, eax
	
	; compute hash for rate command
	push offset str_rate
	call _hash_str
	mov hash_rate, eax
	
	; compute hash for quit command
	push offset str_quit
	call _hash_str
	mov hash_quit, eax

	ret
init_commands ENDP

; main control loop for program which continually prompts user for input
; uses 
command_loop PROC C
_command_loop:

	; promt user for command
	call _insert_new_line
	push offset input_prompt
	call _write_str

	; accept user input
	push offset command_buffer
	call _read_line
	
	; echo user input
	;push offset command_buffer
	;call _write_str

	; parse the command and execute it
	call command_parse_args
	call command_execute

	; add extra line for spacing
	call _insert_new_line

	; run some simulation steps
	;push 1
	;call cmd_step

	; restart command loop
	jmp _command_loop

	; label used to break out of the command loop
	_exit_command_loop:

	ret
command_loop ENDP

; parses an int string at the specified memory address and stores in arg_int
; parameter 1 - pointer to the int string
; uses eax, ebx, ecx, edx
parse_int_arg PROC C
_parse_int_arg:

	; get arguments
	pop eax
	pop edx
	push eax

	; TODO
	mov arg_int, 0
	mov eax, 0 ; stores the current digit character
	mov ecx, 0 ; counts how many digits in the arg string
	mov ebx, 1 ; digit value multiplier

	; loop through each char in the string to see how long before end
	_parse_int_count_loop:
		
		; get the character at the current index
		mov al, [edx+ecx]

		; break the loop when end of string or space is reached
		cmp eax, 0
		je _parse_int_count_loop_break
		cmp eax, ' '
		je _parse_int_count_loop_break
		
		; increment the digit count
		inc ecx
		jmp _parse_int_count_loop

	_parse_int_count_loop_break:

	; loop through each char in the string
	_parse_int_loop:
		
		; get the numerical value of the digit of the current character
		dec ecx
		mov eax, 0
		mov al, [edx+ecx]
		sub al, 48

		push edx
		mul ebx
		pop edx

		; increment total arg int value by digit value
		add arg_int, eax

		; increment digit value multiplier
		imul ebx, ebx, 10

		; loop again if more digits
		cmp ecx, 0
		jg _parse_int_loop

	ret

parse_int_arg ENDP

; parse coord arg from a string at the specified location and return in eax
; parameter 1 - string pointer
; uses eax, ebx, ecx
parse_coord_arg PROC C
_parse_coord_arg:

	; retrieve parameters
	pop eax
	pop ebx ; pointer to string
	push eax

	; reset counter
	mov ecx, 0

	; loop through the characters in the coord string
	_parse_coord_test_loop:

		inc ecx

		; get the current character
		mov edx, 0
		mov dl, [ebx+ecx]

		; break if end of string
		cmp edx, ' '
		je _parse_coord_test_loop_end
		cmp edx, 0
		je _parse_coord_test_loop_end
		
		; if not a digit, fail
		sub dl, '0'
		cmp edx, 9
		jg _parse_coord_fail

		; continue loop if less than 3
		cmp ecx, 3
		jl _parse_coord_test_loop

	_parse_coord_test_loop_end:

	; if one character, fail
	cmp ecx, 1
	jle _parse_coord_fail

	; if less than 4 characters, succeed
	cmp ecx, 3
	jle _parse_coord_fail_end

	_parse_coord_fail:

	; return with an invalid coordinate
	mov eax, 256
	ret

	_parse_coord_fail_end:

	; retrieve the first char of the string
	mov eax, 0
	mov al, [ebx]

	; convert the letter to the column index
	sub al, 'a'

	push eax

	; get the int value after the letter
	add ebx, 1
	push ebx
	call parse_int_arg

	pop eax

	; add the row to the column for the coord index
	mov ebx, arg_int
	shl ebx, 4 ; shift left by 6 is the same as multiply by 16 (row size)
	add eax, ebx

	ret
parse_coord_arg ENDP

; parse the command buffer arguments
; uses eax, ebx, ecx
command_parse_args PROC C
_command_parse_args:

	; reset the int arg
	mov arg_int, 1

	; initialize eax, ebx, and ecx for comparison
	mov eax, 0
	mov ebx, 0

	; check to see if the first characters in the command are digits
	mov al, '0'
	mov bl, command_buffer
	sub bl, al
	cmp ebx, 9
	jg _parse_start_arg_cond_end

	; if first chars are digits, parse them as args
	push offset command_buffer
	call parse_int_arg
	ret

	_parse_start_arg_cond_end:
	
	; reset the loop counter
	mov ecx, 0
	mov eax, 0

	; loop through each character in command buffer
	_parse_args_loop:
		
		; get the character in the buffer at the loop index
		mov al, [command_buffer+ecx]
		inc ecx
		
		; check to see if theres a space
		cmp al, ' '
		je _arg_delimiter_cond
		jmp _arg_delimiter_cond_end

		; if there is a space
		_arg_delimiter_cond:
			
			; cache registers in stack
			push eax
			push ebx
			push ecx

			; store the argument string pointer in ebx and parse it
			mov ebx, offset command_buffer
			add ebx, ecx
			push ebx
			call parse_int_arg

			; restore cached registers
			pop ecx
			pop ebx
			pop eax

		_arg_delimiter_cond_end:

		; loop if not at the string null terminator
		cmp eax, 0
		jne _parse_args_loop

	ret
command_parse_args ENDP

; execute the command based on the first word
; uses eax, ebx
command_execute PROC C
_command_execute:

	; reset eax and ebx for use with ascii character comparisons
	mov eax, 0
	mov ebx, 0

	; check to see if the first characters in the command are digits
	mov al, '0'
	mov bl, command_buffer
	sub bl, al
	cmp ebx, 9
	jle _cmd_exe_step
	
	; get the hash for the command
	push offset command_buffer
	call _hash_str

	; store command hash in ebx
	mov ebx, eax 

	; command step if no input
	cmp ebx, 0
	jle _cmd_exe_step

	; check to see which command was entered
	cmp ebx, hash_help
	je _cmd_exe_help
	cmp ebx, hash_grid
	je _cmd_exe_grid
	cmp ebx, hash_clear
	je _cmd_exe_clear
	cmp ebx, hash_rand
	je _cmd_exe_rand
	cmp ebx, hash_rate
	je _cmd_exe_rate
	cmp ebx, hash_quit
	je _cmd_exe_quit

	; check for implicit coordinate command
	push offset command_buffer
	call parse_coord_arg
	cmp eax, 256
	jge _cmd_invalid

	; if valid coordinate, execute toggle
	mov arg_int, eax
	call cmd_toggle
	jmp _cmd_exe_end

	_cmd_invalid:

	; if no commands entered, echo invalid and skip
	push offset echo_invalid
	call _write_str
	jmp _cmd_exe_end

	_cmd_exe_step:
		push arg_int
		call cmd_step
		jmp _cmd_exe_end
	_cmd_exe_help:
		call cmd_help
		jmp _cmd_exe_end
	_cmd_exe_grid:
		call _reset_cursor
		push 1
		call _clear_space
		push offset echo_grid
		call _write_str
		call cmd_grid
		jmp _cmd_exe_end
	_cmd_exe_clear:
		call _reset_cursor
		push 1
		call _clear_space
		call cmd_clear
		push offset echo_clear
		call _write_str
		call cmd_grid
		jmp _cmd_exe_end
	_cmd_exe_rand:
		call _reset_cursor
		push 1
		call _clear_space
		call cmd_rand
		push offset echo_rand
		call _write_str
		push rand_seed
		call _write_int
		call cmd_grid
		jmp _cmd_exe_end
	_cmd_exe_rate:
		call cmd_rate
		jmp _cmd_exe_end
	_cmd_exe_quit:
		call cmd_quit
	; TODO add rest of commands

	_cmd_exe_end:

	ret
command_execute ENDP

; display the help message
cmd_help PROC C
_cmd_help:
	
	; display help message
	push offset echo_help
	call _write_str

	ret
cmd_help ENDP

; display the grid, clearing all lines that it occupies and before it, 
; except the top line
cmd_grid PROC C
_cmd_grid:
	
	; clear the space before displaying the grid
	call _reset_cursor
	call _insert_new_line
	push 19
	call _clear_space
	
	; display grid state
	call _draw_grid
	
	; clear some space ahead
	push 50
	call _clear_space

	ret
cmd_grid ENDP

; clear any live cells from the grid state
cmd_clear PROC C
_cmd_clear:

	; reset counter
	mov eax, 256

	; target cells to be set to 0
	mov ebx, 0

	; iterate through each grid cell
	_cmd_clear_loop:

		dec eax

		; set the cell at eax to 0
		call _grid_set_cell

		; loop if index greater than 0
		cmp eax, 0
		jg _cmd_clear_loop

	ret
cmd_clear ENDP

; toggle the cell at the arg_int index
; uses eax, ebx
cmd_toggle PROC C
_cmd_toggle:

	; get the cell state of the cell at the arg_int index
	mov eax, arg_int
	call _grid_get_cell

	; store and invert the cell state in ebx, and then put the cell index 
	; back into eax
	mov ebx, eax
	xor ebx, 1
	mov eax, arg_int

	; set the cell to the opposite of its previous state
	call _grid_set_cell ; peeks at eax, bl

	; print echo at the top of the console
	call _reset_cursor
	push 1
	call _clear_space
	call _reset_cursor
	push offset echo_toggle
	call _write_str
	push arg_int
	call _write_int

	; render grid
	call cmd_grid

	ret
cmd_toggle ENDP

; randomize grid state by adding random live cells
cmd_rand PROC C
_cmd_rand:

	; compare to see if a seed is provided
	mov eax, arg_int
	cmp eax, 1
	jne _rand_seed_provided_cond

	; if no seed specified, increment existing seed
	inc rand_seed

	jmp _rand_seed_provided_cond_end

	; if there is a seed provided, use it
	_rand_seed_provided_cond:
		mov rand_seed, eax

	_rand_seed_provided_cond_end:

	; reset counter
	mov ecx, 256

	; prepare randomization seed
	mov eax, rand_seed

	; iterate through each cell in the grid
	_rand_loop:

		; increment and then cache counter
		dec ecx
		push ecx

		; generate random value in eax and cache it in stack
		call _seeded_rand

		; decache counter
		pop ecx
		push eax

		; divide eax by 100 to get random percentage in edx
		mov edx, 0
		mov ebx, 100
		div ebx

		; 10% chance to enable each cell
		cmp edx, 10
		jge _rand_cell_enable_cond_end

		; if chance succeeds, enable grid cell
		mov eax, ecx
		mov ebx, 1
		call _grid_set_cell

		_rand_cell_enable_cond_end:

		; decache seed
		pop eax

		; loop as long as index is valid
		cmp ecx, 0
		jg _rand_loop

	ret
cmd_rand ENDP

; set the animation rate to the int argument
; uses eax
cmd_rate PROC C
_cmd_rate:

	; apply the new animation rate
	mov eax, arg_int
	mov anim_delay, eax

	; echo command output
	push offset echo_rate
	call _write_str
	push anim_delay
	call _write_int

	ret
cmd_rate ENDP

; steps the grid life simulation the specified number of steps
; uses eax, ebx, ecx, edx
; parameter 1 - the amount of steps that the simulation should run for
cmd_step PROC C
_cmd_step:

	pop eax 
	pop ecx ; amount of steps to run
	push eax

	; store back into stack
	push ecx

	; clear the space before beginning the loop
	call _reset_cursor
	push 19
	call _clear_space

	pop ecx ; amount of steps to run

	; loop the specified number of times
	_step_loop:

		push ecx
		push arg_int ; for writing the max step
		push ecx ; for writing the current step

		; progress the game of life simulation grid by one
		call _grid_step
	
		; always draw grid at the top of the console so we dont get 
		; flickering as it scrolls down
		call _reset_cursor

		; write echo grid information
		push offset echo_step
		call _write_str

		; calculate and write steps taken
		pop eax
		mov ecx, arg_int
		sub ecx, eax
		inc ecx
		push ecx
		call _write_int

		push offset echo_step_2
		call _write_str
		call _write_int

		; display grid state
		call _insert_new_line
		call _draw_grid

		; delay before moving onto next step
		push anim_delay
		call _Sleep@4

		; exit the loop if the specified number of iterations have run
		pop ecx
		dec ecx
		cmp ecx, 0
		jg _step_loop
		
	; clear some space ahead
	push 50
	call _clear_space

	ret
cmd_step ENDP

; quit the program
cmd_quit PROC C
_cmd_quit:

	; display exit message
	push offset echo_quit
	call _write_str

	; exit with no error code
	push 0
	call _ExitProcess@4

cmd_quit ENDP

END