; Isaiah Smith
; 4/20/24
; Entry point of program

; 8086 code of version 386
.386P

; specify memory model
.model flat

extern _initialize: near

; import the exit process function from windows kernal with an argument 
; size of 4 bytes
extern _ExitProcess@4: near

; data section is used to store data such as global variables
.data

; code section contains assembly instructions
.code

; entry point for the program that the linker can grab
main PROC near

; main function label
_main:

	call _initialize

	; push 0 onto the stack to use as a parameter in the exit process 
	; function, which will specify the exit code of the program
	push	0
	call	_ExitProcess@4

main ENDP
END