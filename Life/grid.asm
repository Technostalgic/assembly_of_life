; Isaiah Smith
; 4/20/24
; Provide funtionality that handle simulating and interacting with grid for simulation of life

; 8086 code of version 386
.386P

; specify memory model
.model flat

extern _write_chars: near
extern _write_str: near
extern _write_int: near
extern _insert_new_line: near

; data section is used to store data such as global variables
.data

; column labels for the grid
echo_col_lbl	byte	"  a b c d e f g h i j k l m n o p", 0

; the current state of the grid, bytes which represent the bit state of 
; each cell, dimensions are 16 x 16
grid_state		db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,1, 1,1,0,0, 0,0,0,0, 1,0,1,0
				db		0,0,0,1, 0,0,1,0, 0,0,0,1, 0,0,0,0
				db		0,0,0,1, 0,0,0,0, 0,0,0,1, 0,0,0,0
				db		0,0,0,1, 0,0,0,0, 0,0,0,1, 0,0,1,0
				db		0,0,0,0, 1,0,1,0, 0,0,0,1, 1,1,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
				db		0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0

; the transitional state used to store information while the current 
; grid state is stepping forward
grid_state_next	byte	256	DUP(0)

; the grid string that will be used to display the grid in the terminal,
; dimensions are 32 x 16, since two ascii characters are used to represent
; a single grid cell
grid_str		byte	512	DUP(176)

; ascii character constants for rendering
grid_char_empty	byte	176
grid_char_full	byte	219
space_char		byte	' '
space_char_dbl	byte	'  '

; code section contains assembly instructions
.code

; counts the amount of living neighbors at grid cell index specified in al,
; and stores the result back in al
; uses eax, ebx, ecx, edx
grid_count_neighbors PROC C
_grid_count_neighbors:

	; specify horizontal translation factors
	mov cl, 1 ; translate x right
	mov ch, -1 ; translate x left

	; cache eax - the specified cell index
	push eax

	; truncate everything in eax above 8 bits
	and eax, 255

	; decache eax
	pop eax

	; store the index of the specified cell in ebx
	mov ebx, 0
	mov bl, al

	; reset neighbor count to zero
	mov al, 0

	; add all neighbors of that cell together to get the total neighbor count
	
	; not bottom edge
	add bl, 16
	add al, [grid_state+ebx] 
	sub bl, 16

	; not top edge
	sub bl, 16
	add al, [grid_state+ebx] 
	add bl, 16

	; not left or bottom edge
	add bl, 16
	add bl, ch
	add al, [grid_state+ebx] 
	sub bl, 16
	sub bl, ch

	; not left or top edge
	sub bl, 16
	add bl, ch
	add al, [grid_state+ebx] 
	add bl, 16
	sub bl, ch

	; not left edge
	add bl, ch
	add al, [grid_state+ebx] 
	sub bl, ch

	; not right or top edge
	sub bl, 16
	add bl, cl
	add al, [grid_state+ebx] 
	add bl, 16
	sub bl, cl

	; not right or bottom edge
	add bl, 16
	add bl, cl
	add al, [grid_state+ebx]
	sub bl, 16
	sub bl, cl

	; not right edge
	add bl, cl
	add al, [grid_state+ebx]
	sub bl, cl

	ret
grid_count_neighbors ENDP

; takes the index of a cell specified in eax, and calculates the value 
; it should have in the next state, which is then stored in al
; uses eax, ebx, ecx, edx
grid_next_cell_state PROC C
_grid_next_cell_state:

	; store the current state of the specified cell in ah
	mov ah, [grid_state+eax]

	; get live neighbor count and store in eax, without modifying
	; ecx, edx
	push ecx
	push edx
	call grid_count_neighbors
	pop edx
	pop ecx

	; check to see if the specified cell was alive or dead
	cmp ah, 0
	jne _cell_state_live_cond

	; if the specified cell is dead
	; check to see if the cell should come to life
	cmp al, 3
	je _cell_dead_cond_live

	; if cell should not come back to life, set dead
	mov al, 0
	jmp _cell_dead_cond_end

	; if cell should come back to life, set alive
	_cell_dead_cond_live:
		mov eax, 1

	_cell_dead_cond_end:

	; skip alive condition
	jmp _cell_state_cond_end

	; if the specified cell is alive
	_cell_state_live_cond:
	
		; check to see if the cell should stay alive or die
		cmp al, 2
		jl _cell_live_cond_die
		cmp al, 3
		jg _cell_live_cond_die

		; if the cell should stay alive, set alive
		mov eax, 1
		jmp _cell_live_cond_end

		; if the cell should die, set dead
		_cell_live_cond_die:
			mov eax, 0

		_cell_live_cond_end:

	_cell_state_cond_end:

	ret
grid_next_cell_state ENDP

; updates the grid_state_next to represent the next iteration of each of
; the cells in the current grid_state
; uses eax, ebx, ecx
update_grid_state_next PROC C
_update_grid_state_next:

	; clear all bits in eax
	mov eax, 0

	; reset loop counter to the amount of grid cells in the grid
	mov ecx, 256

	; loop through each grid cell
	_grid_state_next_loop:

		dec ecx

		; calculate the value for the next state at each cell and store it in 
		; the next grid state at the proper index
		mov eax, ecx
		call grid_next_cell_state ; put cell next state in eax, uses ebx
		mov [grid_state_next+ecx], al

		; exit loop if all cells have been iterated through
		cmp ecx, 0
		jg _grid_state_next_loop

	ret
update_grid_state_next ENDP

; grabs the next state from grid_state_next, and applies it to the current
; grid state
; uses eax, ecx
update_grid_state PROC C
_update_grid_state:

	; clear all bits in eax
	mov eax, 0

	; reset loop counter to the amount of grid cells in the grid
	mov ecx, 256
	
	; loop through each grid cell in next state
	_grid_state_loop:
		
		dec ecx

		; apply the current cell next grid state to the current grid state
		mov al, [grid_state_next+ecx]
		mov [grid_state+ecx], al

		; exit loop if all cells have been iterated
		cmp ecx, 0
		jg _grid_state_loop

	ret
update_grid_state ENDP

; uses eax, ebx, ecx
grid_step PROC C
_grid_step:

	; update the next grid state then apply it to the current grid state
	call update_grid_state_next ; eax, ebx, ecx
	call update_grid_state ; eax, ecx

	ret
grid_step ENDP

; get whether the grid cell at the index specified in eax is alive (1) 
; or dead (0) and store in al
; uses eax
grid_get_cell PROC C
_grid_get_cell:
	mov al, [grid_state+eax]
	ret
grid_get_cell ENDP

; set the cell at the index specified in eax to the value in bl
grid_set_cell PROC C
_grid_set_cell:
	mov [grid_state+eax], bl
	ret
grid_set_cell ENDP

; fills out the grid str based on the grid cell data in grid state
; uses eax, ebx, ecx
update_grid_str PROC C
_update_grid_str:

	; set loop counter to amount of cells in grid
	mov ecx, 256

	; loop over each cell in grid
	_update_grid_loop:
	
		dec ecx

		; check if grid cell is empty
		mov ebx, 0
		mov bl, [grid_state+ecx]
		cmp bl, 0
		je _grid_cell_empty_cond

		; if grid cell is not empty, use the 'full' character
		_grid_cell_full_cond:
			mov bl, grid_char_full
			jmp _grid_cell_cond_end

		; if grid cell is empty, use the 'empty' character
		_grid_cell_empty_cond:
			mov bl, grid_char_empty
			jmp _grid_cell_cond_end

		_grid_cell_cond_end:

		; write the approriate character to the grid string at the proper indices
		imul eax, ecx, 2
		mov [grid_str+eax], bl
		mov [grid_str+eax+1], bl

		; break after end of grid has been reached
		cmp ecx, 0
		jg _update_grid_loop

	ret
update_grid_str ENDP

; draws the grid ascii graphics into the console
; uses eax, ebx, ecx,
draw_grid PROC C
_draw_grid:

	call update_grid_str

	; add a line break
	call _insert_new_line

	; reset counter for loop
	mov ebx, 0

	; add column labels
	push offset echo_col_lbl
	call _write_str

	; add a line break
	call _insert_new_line

	; draw each row of grid string
	_draw_grid_loop:
		
		; calculate the index offset from the grid string
		mov eax, ebx
		imul eax, 32

		; get the pointer to the character starting at the correct row
		lea ecx, grid_str
		add eax, ecx

		; cache registers for function calls
		push eax
		push ecx
		push ebx

		; write row number
		pop ebx
		push ebx
		push ebx
		call _write_int

		; restor ebx from stack cache
		pop ebx

		; check if iteration is 10 or greater
		cmp ebx, 10
		jge _draw_grid_loop_continue
		
		; add spacing if only single digit row number
		push offset space_char
		push 1
		call _write_chars

		_draw_grid_loop_continue:

		; restore registers from stack cache
		pop ecx
		pop eax

		; draw current row of ascii chars
		push eax
		push 32
		call _write_chars

		; write line break
		call _insert_new_line

		; break loop after 16 iterations
		inc ebx
		cmp ebx, 16
		jl _draw_grid_loop

	ret
draw_grid ENDP

END