; Isaiah Smith
; 4/20/24
; Functionality to write/read from console

; 8086 code of version 386
.386P

; specify memory model
.model flat

; Import Get Standard Handle function from Windows kernel
extern  _GetStdHandle@4: near

; Import Write To Console function from Windows console lib
extern  _WriteConsoleA@20: near

; Import Read Console function from Windows console lib
extern  _ReadConsoleA@20: near

; Import function to fill console with char
extern _FillConsoleOutputCharacterA@20: near

; Import function to set console cursor position
extern _SetConsoleCursorPosition@8: near

; Import function for getting console buffer info
extern _GetConsoleScreenBufferInfo@8: near

; Import function for getting cursor info
extern _GetConsoleCursorInfo@8: near

; data section is used to store data such as global variables
.data

outputHandle		dword	?
inputHandle			dword	?

written				dword	?
readBuffer			byte	1024	DUP(0)
numCharsToRead		dword	1024
numCharsRead		dword	100

cursor_pos			dword	?
clr_space_ret_ptr	dword	?

; console buffer info struct
cons_size_x			word	?
cons_size_y			word	?
cons_cursor_x		word	?
cons_cursor_y		word	?
cons_unused			word	16	DUP(0)

; special characters for rendering
char_line_break		byte	10
char_space			byte	' '
char_backspace		byte	8
char_space_buffer	byte	1024	DUP(' ')

; to store digits in so that we can write int
buffer_digits		byte	16	DUP(0)

; code section contains assembly instructions
.code

init_io PROC C
_init_io:
	
	; get output handle for console output
	push	-11
	call	_GetStdHandle@4
	mov		outputHandle, eax

	; get input handle for console input
	push	-10
	call	_GetStdHandle@4
	mov		inputHandle, eax

	ret

init_io ENDP

; reset the cursor position to the beginning of the console
reset_cursor PROC C
_reset_cursor:

	; set parameters and call function
	push 0 ; cursor coordinate
	push outputHandle
	call _SetConsoleCursorPosition@8

	ret
reset_cursor ENDP

; calculate the hash value of a string and return it in eax so that it 
; can be compared to another (hash breaks at first space or null)
; parameter 1 - pointer to the string to be hashed
; uses eax, ebx, ecx, edx
hash_str PROC C
_hash_str:

	; get parameters
	pop eax
	pop ebx ; string pointer
	push eax
	
	; reset hash counter
	mov eax, 0
	mov ecx, 0
	mov edx, 0
	
	; loop through all characters in string
	_hash_loop:

		mov dl, [ebx+ecx] ; current string character

		; break when end of string or space is reached
		inc ecx
		cmp edx, 0
		je _hash_loop_break
		cmp edx, ' '
		je _hash_loop_break

		; increment the hash based on the character value
		add eax, edx

		jmp _hash_loop

	_hash_loop_break:

	ret
hash_str ENDP

; clear the specified number of spaces ahead of the cursor
; uses eax, ebx, ecx
; parameter 1 - amount of lines to clear
clear_space PROC C
_clear_space:

	pop clr_space_ret_ptr
	pop ebx ; amount of lines
	
	; cache the current cursor pos
	push offset cons_size_x
	push outputHandle
	call _GetConsoleScreenBufferInfo@8

	; cache the cursor position
	mov eax, 0
	mov ax, cons_cursor_y
	shl eax, 16
	mov ax, cons_cursor_x
	mov cursor_pos, eax
	
	; loop the same amount of times as how many lines to be cleared
	_clear_space_loop:

		; get console buffer char width
		mov eax, 0
		mov ax, cons_size_x

		; clear the line
		push offset char_space_buffer
		push eax
		call write_chars
	
		; go to next line
		push offset char_line_break
		push 1
		call write_chars

		; break once all lines have been cleared
		dec ebx
		cmp ebx, 0
		jg _clear_space_loop

	; scroll to top
	call reset_cursor

	; restore the previous cursor position
	push cursor_pos
	push outputHandle
	call _SetConsoleCursorPosition@8

	push clr_space_ret_ptr
	ret
clear_space ENDP

; inserts a new line, pretty self explanatory
insert_new_line PROC C
_insert_new_line:

	push offset char_line_break
	push 1
	call write_chars

	ret
insert_new_line ENDP

; define assembler function readline
; ReadConsole(handle, &buffer, numCharToRead, numCharsRead, null)
read_line PROC C
_read_line: 

	pop eax
	pop ebx ; pointer to the read buffer
	push eax
	push ebx

	; first parameter is unused so we set it to null
	push	0

	; point to numCharsRead which will store the number of how many 
	; characters were read from the user input when the function is called
	push	offset numCharsRead

	; point to numCharsToRead, which should be the size of the readBuffer
	; so that the function will only up until this many characters if a null
	; teminator character is not reached by then
	push	numCharsToRead

	; point to readBuffer which is where the data that is read from the 
	; console will be stored
	push	ebx

	; set the last parameter to inputHandle and call the function
	push	inputHandle
	call	_ReadConsoleA@20

	; remove the new line character from the read buffer
	pop ebx ; pointer to read buffer
	add ebx, numCharsRead
	mov byte ptr [ebx-2], 0

	; return to previous location in call stack
	ret
read_line ENDP


; writes the specified amount of characters of an ascii sring at the specified 
; memory location to console output
; uses eax, ecx, edx
; parameter 1 - pointer to ascii string
; parameter 2 - amount of chars to output
write_chars PROC C
_write_chars:
	
	pop eax
	pop ecx ; character count
	pop edx ; string pointer

	; return call stack pointer to stack
	push eax 

	; WriteConsole(handle, &msg[0], numCharsToWrite, &written, 0)
	; this call outputs the contents of the 'readbuffer' array into the 
	; console, up to the number specified by numCharsRead
	push 0
	push offset written
	push ecx					; amount of characters to write
	push edx					; points to first parameter
	push outputHandle
	call _WriteConsoleA@20

	ret
write_chars ENDP

; writes a full ascii string to console until a null terminator character 
; is reached
; uses eax, ebx, ecx, edx
; parameter 1 - pointer to ascii string
write_str PROC C
_write_str:

	; pop the top 2 elements off the stack so they can be rearranged into the 
	; proper order for use as paramaters with the WriteConsole function
	pop eax
	pop edx ; string pointer

	; reset character counter
	mov ecx, 0
	mov ebx, 0

	; count the amount of characters in the string before the null terminator
	_char_count_loop:
		mov bl, BYTE PTR [edx+ecx]
		inc ecx
		cmp ebx, 0
		jne _char_count_loop
	dec ecx

	; return call stack pointer to stack
	push eax

	; write the string with the calculated char count
	push edx ; string pointer
	push ecx ; char count
	call write_chars

	ret
write_str ENDP

; writes an int from the stack to the console output
; parameter 1 - int, the number to output
; uses eax, ebx, ecx, edx
write_int PROC C
_write_int:
	
	; get parameters
	pop eax
	pop ebx ; the number to write
	push eax

	; reset counter
	mov ecx, 0
	mov eax, ebx ; the number to write

	; count digits
	_write_int_count_loop:

		; integer divide eax by 10
		mov edx, 0 ; reset remainder
		mov ebx, 10 ; set divisor
		div ebx

		; get the ascii character corresponding to the digit
		mov ebx, '0'
		add ebx, edx

		; store the ascii digit in buffer
		mov [buffer_digits+ecx], bl
	
		; increment counter
		inc ecx

		; when it reaches zero that means it was the last digit
		cmp eax, 0
		jg _write_int_count_loop

	_write_int_loop:
		
		; decrement the counter
		dec ecx

		; store ecx between calls
		push ecx

		; write digits in buffer
		mov ebx, 0
		lea ebx, buffer_digits
		add ebx, ecx
		push ebx
		push 1
		call write_chars
		
		; restore counter
		pop ecx

		; break loop when ecx has rechead zero
		cmp ecx, 0
		jle _break_write_int_loop

		jmp _write_int_loop

	_break_write_int_loop:

	ret
write_int ENDP

; uses lcg algorithm to generate a psuedorandom seeded number using eax as 
; the seed and returns the random value in eax
; uses eax, ebx, ecx, edx
seeded_rand PROC C
_seeded_rand:
	mov ebx, 1103515245
	mov ecx, 123
	mul ebx
	add eax, ecx
	ret
seeded_rand ENDP

END